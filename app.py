import os
import requests

from flask import Flask, render_template, request, url_for, flash, redirect, send_from_directory
from werkzeug.exceptions import abort

app = Flask(__name__)
app.config['SECRET_KEY'] = 'nmDktCR3w7DXuK!Pm4$MFpemJ'

backend_endpoint = 'http://' + os.environ['BACKEND']

def list_stations():
    headers = {'Content-type': 'application/json'}
    r = requests.get('{0}/api/stations'.format(backend_endpoint), headers=headers)
    json_response = r.json()
    if r.status_code == 200:
        data = json_response
    else:
        print('Error! Returned status code %s' % r.status_code)
        print('Message: %s' % json_response['error']['message'])
        print('Reason: %s' % json_response['error']['reason'])
    return data

def get_station(station_id):
    headers = {'Content-type': 'application/json'}
    r = requests.get('{0}/api/station/{1}'.format(backend_endpoint, station_id), headers=headers)
    json_response = r.json()
    if r.status_code == 200:
        data = json_response
    else:
        print('Error! Returned status code %s' % r.status_code)
        print('Message: %s' % json_response['error']['message'])
        print('Reason: %s' % json_response['error']['reason'])
    return data

def get_winds(station_id, dataframe):
    headers = {'Content-type': 'application/json'}
    r = requests.get('{0}/api/wind/{1}/{2}'.format(backend_endpoint, station_id, dataframe), headers=headers)
    json_response = r.json()
    if r.status_code == 200:
        data = json_response
    else:
        print('Error! Returned status code %s' % r.status_code)
        print('Message: %s' % json_response['error']['message'])
        print('Reason: %s' % json_response['error']['reason'])
    return data

def get_wind(station_id, dataframe, wind_id):
    headers = {'Content-type': 'application/json'}
    r = requests.get('{0}/api/wind/{1}/{2}/{3}'.format(backend_endpoint, station_id, dataframe, wind_id), headers=headers)
    json_response = r.json()
    if r.status_code == 200:
        data = json_response
    else:
        print('Error! Returned status code %s' % r.status_code)
        print('Message: %s' % json_response['error']['message'])
        print('Reason: %s' % json_response['error']['reason'])
    return data

def get_dataframes(station_id):
    headers = {'Content-type': 'application/json'}
    r = requests.get('{0}/api/wind/{1}'.format(backend_endpoint, station_id), headers=headers)
    json_response = r.json()
    if r.status_code == 200:
        data = json_response
    else:
        print('Error! Returned status code %s' % r.status_code)
        print('Message: %s' % json_response['error']['message'])
        print('Reason: %s' % json_response['error']['reason'])
    return data  

@app.route('/')
def index():
    stations = list_stations()
    return render_template('index.html', stations=stations['stations'])

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                          'favicon.ico',mimetype='image/vnd.microsoft.icon')

@app.route('/<station_id>')
def station(station_id):
    station_by_id = get_station(station_id)
    dataframes = get_dataframes(station_id)
    return render_template('station.html', station=station_by_id['station'], dataframes=dataframes.get('dataframes'))

@app.route('/<station_id>/<dataframe>/winds')
def wind(station_id, dataframe):
    winds = get_winds(station_id, dataframe)
    return render_template('wind.html', station=station_id, dataframe=dataframe, winds=winds.get('winds'))

@app.route('/create_station', methods=('GET', 'POST'))
def create_station():
    if request.method == 'POST':

        if not request.form['station_id']:
            flash('Station ID is required!')
        else:
            parameters = {
                'station_id': request.form['station_id'],
                'name': request.form['station_name']
            }
            headers = {'Content-type': 'application/json'}
            r = requests.post('{0}/api/station'.format(backend_endpoint), json=parameters, headers=headers)
            json_response = r.json()

            if r.status_code == 201:
                print('Station {0} added'.format(request.form['station_id']))
            else:
                print('Error! Returned status code %s' % r.status_code)
                print('Message: %s' % json_response['error']['message'])
                print('Reason: %s' % json_response['error']['reason'])
            return redirect(url_for('index'))

    return render_template('create_station.html')

@app.route('/<station_id>/create_dataframe', methods=('GET', 'POST'))
def create_dataframe(station_id):
    if request.method == 'POST':
        parameters = {
            'station_id': station_id,
            'dataframe': request.form['dataframe']
        }
        headers = {'Content-type': 'application/json'}
        r = requests.post('{0}/api/dataframe'.format(backend_endpoint), json=parameters, headers=headers)
        json_response = r.json()

        if r.status_code == 201:
            print('Dataframe {0} added'.format(request.form['dataframe']))
        else:
            print('Error! Returned status code %s' % r.status_code)
            print('Message: %s' % json_response['error']['message'])
            print('Reason: %s' % json_response['error']['reason'])
        return redirect(url_for('index'))

    return render_template('create_dataframe.html')

@app.route('/<station_id>/<dataframe>/create_wind', methods=('GET', 'POST'))
def create_wind(station_id, dataframe):
    if request.method == 'POST':
        parameters = {
            'dataframe': dataframe,
            'time': request.form['time'],
            'wind': request.form['wind']
        }
        headers = {'Content-type': 'application/json'}
        r = requests.post('{0}/api/wind'.format(backend_endpoint), json=parameters, headers=headers)
        json_response = r.json()
        
        if r.status_code == 201:
            print('Wind {0} {1} added'.format(request.form['wind'], request.form['time']))
        else:
            print('Error! Returned status code %s' % r.status_code)
            print('Message: %s' % json_response['error']['message'])
            print('Reason: %s' % json_response['error']['reason'])
        return redirect(url_for('index'))

    return render_template('create_wind.html')

@app.route('/<station_id>/edit', methods=('GET', 'POST'))
def edit_station(station_id):
    station = get_station(station_id)

    if request.method == 'POST':
        if not request.form['station_id']:
            flash('Station ID is required!')
        else:
            parameters = {
                'station_id': request.form['station_id'],
                'name': request.form['station_name']
            }

            headers = {'Content-type': 'application/json'}
            r = requests.put('{0}/api/station/{1}'.format(backend_endpoint, station_id), json=parameters, headers=headers)
            json_response = r.json()

            if r.status_code == 201:
                print('Station {0} edited'.format(request.form['station_id']))
            else:
                print('Error! Returned status code %s' % r.status_code)
                print('Message: %s' % json_response['error']['message'])
                print('Reason: %s' % json_response['error']['reason'])
            return redirect(url_for('index'))

    return render_template('edit_station.html', station=station.get('station'))

@app.route('/<station_id>/delete', methods=('POST',))
def delete_station(station_id):
    if request.method == 'POST':

        parameters = {
            'delete': 'true'
        }
        headers = {'Content-type': 'application/json'}
        r = requests.delete('{0}/api/station/{1}'.format(backend_endpoint, station_id), json=parameters, headers=headers)
        json_response = r.json()

        if r.status_code == 201:
            print('Station {0} deleted'.format(station_id))
        else:
            print('Error! Returned status code %s' % r.status_code)
            print('Message: %s' % json_response['error']['message'])
            print('Reason: %s' % json_response['error']['reason'])
        flash('"{}" was successfully deleted!'.format(station_id))
        return redirect(url_for('index'))

@app.route('/<station_id>/<dataframe>/<wind_id>/edit_wind', methods=('GET', 'POST'))
def edit_wind(station_id, dataframe, wind_id):
    wind = get_wind(station_id, dataframe, wind_id)
    print(wind.get('wind')[0])

    if request.method == 'POST':
        parameters = {
            'dataframe': dataframe,
            'time': request.form['time'],
            'wind': request.form['wind']
        }
        headers = {'Content-type': 'application/json'}
        r = requests.put('{0}/api/wind/{1}/{2}/{3}'.format(backend_endpoint, station_id, dataframe, wind_id), json=parameters, headers=headers)
        json_response = r.json()
        
        if r.status_code == 201:
            print('Wind {0} {1} edited'.format(request.form['wind'], request.form['time']))
        else:
            print('Error! Returned status code %s' % r.status_code)
            print('Message: %s' % json_response['error']['message'])
            print('Reason: %s' % json_response['error']['reason'])
        return redirect(url_for('index'))

    return render_template('edit_wind.html', station_id=station_id, dataframe=dataframe, wind=wind.get('wind')[0])

@app.route('/<station_id>/<dataframe>/<wind_id>/delete', methods=('POST',))
def delete_wind(station_id, dataframe, wind_id):
    if request.method == 'POST':

        parameters = {
            'delete': 'true'
        }
        headers = {'Content-type': 'application/json'}
        r = requests.delete('{0}/api/wind/{1}/{2}/{3}'.format(backend_endpoint, station_id, dataframe, wind_id), json=parameters, headers=headers)
        json_response = r.json()

        if r.status_code == 201:
            print('Wind {0} deleted'.format(wind_id))
        else:
            print('Error! Returned status code %s' % r.status_code)
            print('Message: %s' % json_response['error']['message'])
            print('Reason: %s' % json_response['error']['reason'])
        flash('"{}" was successfully deleted!'.format(wind_id))
        return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port='5000')
